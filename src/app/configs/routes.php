<?php

/*
|----------------------------------------------
| Routes Configuration
|----------------------------------------------
| 
| Here is where you can configuration 
| url web routes for your application.
*/
require_once '/home/app/src/app/controllers/BlogController.php';


$router->get('/', [App\Controllers\BlogController::class, 'index']);

$router->get('/user/{user}', [\App\Controllers\UserController::class, 'show']);
//TODO create Other RestFul API functions ....
$router->get('/user/', [\App\Controllers\UserController::class, 'register']);
$router->get('/user/', [\App\Controllers\UserController::class, 'update']);
$router->get('/user/{user}', [\App\Controllers\UserController::class, 'delete']);

$router->get('/article/{article}', [\App\Controllers\UserController::class, 'show']);
//TODO create Other RestFul API functions ....
$router->get('/article/', [\App\Controllers\ArticleController::class, 'store']);
$router->get('/article/', [\App\Controllers\ArticleController::class, 'update']);
$router->get('/article/{article}', [\App\Controllers\ArticleController::class, 'delete']);
