<?php 

namespace App\Controllers;

use Core\Controller;
use Core\ModelFactory;


class UserController extends Controller
{
    /**
     * @return void
     */
    public function index()
    {
        ModelFactory::model('User')->all();
    }

    /**
     * @return void
     */
    public function register()
    {
        $data = [
            'name' => 'pegah esfehani',
            'email' => 'pegah@gmail.com',
            'password' => password_hash('secret', PASSWORD_BCRYPT),
        ];

        ModelFactory::model('User')->create($data);
    }

    /**
     * @param $id
     * @return void
     */
    public function show($id)
    {
        $select_data = ['email', 'password'];

        $where = ['user_id' => $id];

        ModelFactory::model('User')->get($select_data, $where);
    }

    /**
     * @param $id
     * @return void
     */
    public function update($id)
    {
        $data = [
            'password' => password_hash('testing', PASSWORD_BCRYPT),
        ];

        ModelFactory::model('User')->update($data, $id);
    }

    /**
     * @param $id
     * @return void
     */
    public function delete($id)
    {
        ModelFactory::model('User')->delete($id);
    }
}
