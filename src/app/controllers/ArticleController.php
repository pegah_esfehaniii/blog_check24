<?php

namespace App\Controllers;

use Core\Controller;
use Core\ModelFactory;


class ArticleController extends Controller
{
    /**
     * @return void
     */
    public function index()
    {
        ModelFactory::model('Article')->all();
    }

    /**
     * @return void
     */
    public function store()
    {
        $data = [
            'user_id' => 1,
            'text' => 'this is text',
            'status' => 'show',
        ];

        ModelFactory::model('Article')->create($data);
    }

    /**
     * Display the specified data from storage.
     *
     * @param $id
     */
    public function show($id)
    {
        $select_data = ['text', 'status'];

        $where = ['id' => $id];

        ModelFactory::model('Article')->get($select_data, $where);
    }

    /**
     * Update the specified data in storage.
     *
     * @param $id
     */
    public function update($id)
    {
        $data = [
            'text' => 'this is text',
            'status' => 'show',
        ];

        ModelFactory::model('Article')->update($data, $id);
    }

    /**
     * Delete the specified data from storage.
     *
     * @param $id
     */
    public function delete($id)
    {
        ModelFactory::model('Article')->delete($id);
    }
}
