<?php 

namespace App\Controllers;

require_once '/home/app/src/core/Controller.php';
use Core\Controller;

/**
 * Example Homepage Controller
 */
class BlogController extends Controller
{
    /**
     * @return void
     */
    public function index()
    {
        $this->view('home');
    }

}
