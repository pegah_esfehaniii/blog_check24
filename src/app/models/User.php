<?php

namespace App\Models;
require_once '/home/app/src/core/model.php';

use Core\Model;


class User extends Model
{
    protected $table = 'users';

    protected $primary_key = 'user_id';

    protected $fillable = [
        'username', 'password'
    ];
}
