<?php 

namespace Core;

class ModelFactory
{
    /**
     * Accessing a model in controller.
     * 
     * @param $name
     */
    static public function model($name)
    {
        $class = '\App\Models\\'.$name;
        return new $class;
    }
}
