<?php 

namespace Core;

use PDO;

class Database
{
    /**
     * @var
     */
    private static $_connection;

    /**
     * @var
     */
    private static $_host;

    /**
     * @var
     */
    private static $_dbname;

    /**
     * @var
     */
    private static $_username;

    /**
     * @var
     */
    private static $_password;

    /**
     * @var
     */
    protected static $instance;

    /**
     *
     */
    private function __construct() {
                $config = self::$_connection.
        ':host='.self::$_host.
        ';dbname='.self::$_dbname;

        $conn = new PDO($config, self::$_username, self::$_password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        return $conn;
    }

    /**
     * @return mixed
     */
    public static function getInstance() {

        if (!self::$instance) {

            self::$_connection = getenv('DB_CONNECTION');
            self::$_host = getenv('DB_HOST');
            self::$_dbname = getenv('DB_DATABASE');
            self::$_username = getenv('DB_USERNAME');
            self::$_password = getenv('DB_PASSWORD');

            new Database();
        }

        return self::$instance;
    }


}
